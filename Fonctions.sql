CREATE OR REPLACE PROCEDURE insert_player(
    fn text, ln text, nn varchar(16), team_id bigint
) AS $$
    DECLARE
        member_id bigint;
    BEGIN
        INSERT INTO member VALUES (DEFAULT, fn, ln, nn, team_id) RETURNING member.id INTO member_id;
        INSERT INTO player VALUES (member_id);
    END;
$$ LANGUAGE plpgsql;
END;

CREATE OR REPLACE PROCEDURE insert_player(
    fn text, ln text, nn varchar(16), team_name varchar(16)
) AS $$
    DECLARE
        team_id bigint;
        member_id bigint;
    BEGIN
        team_id := (SELECT team.id FROM team WHERE team.name = team_name);
        if team_id IS NULL then
            INSERT INTO team VALUES (DEFAULT, team_name) RETURNING team.id INTO team_id;
        end if;
        INSERT INTO member VALUES (DEFAULT, fn, ln, nn, team_id) RETURNING member.id INTO member_id;
        INSERT INTO player VALUES (member_id);
    END;
$$ LANGUAGE plpgsql;
END;

CREATE OR REPLACE PROCEDURE insert_demolition(
    gid bigint, t time, dmd bigint, dmr bigint
) AS $$
    DECLARE
        event_id bigint;
    BEGIN
        INSERT INTO game_event VALUES (DEFAULT, gid, t) RETURNING game_event.id INTO event_id;
        INSERT INTO demolition VALUES (event_id, dmd, dmr);
    END;
$$ LANGUAGE plpgsql;
END;

CREATE OR REPLACE PROCEDURE insert_fifty(
    gid bigint, t time, win bigint, loo bigint
) AS $$
    DECLARE
        event_id bigint;
    BEGIN
        INSERT INTO game_event VALUES (DEFAULT, gid, t) RETURNING game_event.id INTO event_id;
        INSERT INTO fifty VALUES (event_id, win, loo);
    END;
$$ LANGUAGE plpgsql;
END;

CREATE OR REPLACE PROCEDURE insert_goal(
    gid bigint, t time, scr bigint
) AS $$
    DECLARE
        event_id bigint;
    BEGIN
        INSERT INTO game_event VALUES (DEFAULT, gid, t) RETURNING game_event.id INTO event_id;
        INSERT INTO goal VALUES (event_id, scr);
    END;
$$ LANGUAGE plpgsql;
END;

CREATE OR REPLACE PROCEDURE insert_save(
    gid bigint, t time, gpr bigint
) AS $$
    DECLARE
        event_id bigint;
    BEGIN
        INSERT INTO game_event VALUES (DEFAULT, gid, t) RETURNING game_event.id INTO event_id;
        INSERT INTO save VALUES (event_id, gpr);
    END;
$$ LANGUAGE plpgsql;
END;

CREATE OR REPLACE PROCEDURE insert_shoot(
    gid bigint, t time, scr bigint
) AS $$
    DECLARE
        event_id bigint;
    BEGIN
        INSERT INTO game_event VALUES (DEFAULT, gid, t) RETURNING game_event.id INTO event_id;
        INSERT INTO shoot VALUES (event_id, scr);
    END;
$$ LANGUAGE plpgsql;
END;
