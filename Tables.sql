CREATE TABLE IF NOT EXISTS team(
    id bigserial PRIMARY KEY,
    name varchar(16) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS member(
    id         bigserial PRIMARY KEY,
    first_name text NOT NULL,
    last_name  text NOT NULL,
    nickname   varchar(16) UNIQUE NOT NULL,
    team       bigserial REFERENCES team(id) NOT NULL
);

CREATE TABLE IF NOT EXISTS player(
    id bigserial PRIMARY KEY REFERENCES member(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS coach(
    id bigserial PRIMARY KEY REFERENCES member(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS game(
    id bigserial PRIMARY KEY,
    blue_team bigserial NOT NULL REFERENCES team(id),
    red_team bigserial NOT NULL REFERENCES team(id),
    start_date timestamp NOT NULL,
    end_date timestamp NOT NULL
);

CREATE TABLE IF NOT EXISTS game_event(
    id bigserial PRIMARY KEY,
    game_id bigserial NOT NULL REFERENCES game(id) ON DELETE CASCADE,
    time time NOT NULL
);

CREATE TABLE IF NOT EXISTS shoot(
    id bigserial PRIMARY KEY REFERENCES game_event(id) ON DELETE CASCADE,
    shooter bigserial NOT NULL REFERENCES player(id)
);

CREATE TABLE IF NOT EXISTS save(
    id bigserial PRIMARY KEY REFERENCES game_event(id) ON DELETE CASCADE,
    goalkeeper bigserial NOT NULL REFERENCES player(id)
);

CREATE TABLE IF NOT EXISTS fifty(
    id bigserial PRIMARY KEY REFERENCES game_event(id) ON DELETE CASCADE,
    winner bigserial REFERENCES player(id),
    looser bigserial REFERENCES player(id)
);

CREATE TABLE IF NOT EXISTS goal(
    id bigserial PRIMARY KEY REFERENCES game_event(id) ON DELETE CASCADE,
    scorer bigserial NOT NULL REFERENCES player(id)
);

CREATE TABLE IF NOT EXISTS demolition(
    id bigserial PRIMARY KEY REFERENCES game_event(id) ON DELETE CASCADE,
    demolished bigserial NOT NULL REFERENCES player(id),
    demolisher bigserial NOT NULL REFERENCES player(id)
);
