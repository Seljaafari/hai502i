CALL insert_player('Samy', 'EL-JAAFARI', 'Jaff', 'Karmine');
CALL insert_player('Sébastien', 'VIAL', 'Shyrogan', 'Solary');

INSERT INTO game VALUES (DEFAULT, 1, 2, '2022-11-11 20:00:00', '2022-11-11 20:06:00');
CALL insert_goal(1, '1:00', 2);
CALL insert_goal(1, '2:00', 2);
CALL insert_goal(1, '4:00', 1);

SELECT m.nickname, t.name as team, COUNT(*) as total_goals FROM player p, member m, goal g, team t
                            WHERE p.id = m.id AND g.scorer = p.id AND t.id = m.team
                            GROUP BY (m.id, m.nickname, t.name);